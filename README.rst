++++++++++++
pyAggr3g470r
++++++++++++

Presentation
============

`pyAggr3g470r <https://bitbucket.org/cedricbonhomme/pyaggr3g470r>`_ is a
web-based news aggregator.

Features
========

* can be deployed on Heroku or on a traditional server;
* multiple users can use the platform;
* a RESTful API to manage your articles;
* data liberation: export and import all your account with a JSON file;
* export and import feeds with OPML files;
* export articles to HTML;
* favorite articles;
* detection of inactive feeds;
* share articles with Google +, Pinboard and reddit;
* HTTP proxy support.

The core technologies are `Flask <http://flask.pocoo.org>`_,
`asyncio <https://www.python.org/dev/peps/pep-3156/>`_ and
`SQLAlchemy <http://www.sqlalchemy.org>`_.

Python 3.4 is recommended.

Documentation
=============

A documentation is available `here <https://pyaggr3g470r.readthedocs.org>`_.

Internationalization
====================

pyAggr3g470r is translated into English and French.

Donation
========

If you wish and if you like *pyAggr3g470r*, you can donate via bitcoin
`1GVmhR9fbBeEh7rP1qNq76jWArDdDQ3otZ <https://blockexplorer.com/address/1GVmhR9fbBeEh7rP1qNq76jWArDdDQ3otZ>`_.
Thank you!

License
=======

`pyAggr3g470r <https://bitbucket.org/cedricbonhomme/pyaggr3g470r>`_
is under the `GNU Affero General Public License version 3 <https://www.gnu.org/licenses/agpl-3.0.html>`_.

Contact
=======

`My home page <https://www.cedricbonhomme.org>`_.
