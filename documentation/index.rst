.. pyAggr3g470r documentation master file, created by
   sphinx-quickstart on Fri Feb 20 07:23:42 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pyAggr3g470r's documentation!
========================================


Configuration and deployment
============================

.. toctree::
    :maxdepth: 2

    requirements
    deployment

Web services
============

.. toctree::
    :maxdepth: 2

    web-services

Migrations
==========

.. toctree::
    :maxdepth: 2

    migrations


